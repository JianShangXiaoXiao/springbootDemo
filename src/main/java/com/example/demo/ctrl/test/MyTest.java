package com.example.demo.ctrl.test;

import com.example.demo.ctrl.dto.School;
import com.example.demo.ctrl.dto.User;
import com.example.demo.ctrl.util.LogToString;

/**
 * 测试引用传递
 */
public class MyTest {

    public static void main(String[] args) {
        User user=new User();
        user.setSex("男");
        School s=new School();
        s.setUser(user);
        System.out.println("1--结果："+ LogToString.objectToString(s));
        User user2=s.getUser();
        MyTest m=new MyTest();
        //这个方法改变了内部对象的属性值
        m.tt(s);
        System.out.println("2--结果："+ LogToString.objectToString(s));
        System.out.println("3--结果："+ LogToString.objectToString(user2));
    }

    public void tt(School s){
        User user2=s.getUser();
        user2.setSex("女");
    }
}
