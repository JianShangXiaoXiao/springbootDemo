package com.example.demo.ctrl.test.java8;
/*接口*/
public interface Apple {
    //抽象方法，默认为public abstract修饰，所有的jdk都支持接口写抽象方法，需要子类实现
    void add();

    /* 普通方法：jdk8新加支持普通方法写方法体，不需要子类实现，jdk8之前不支持
    * 默认方法，支持写方法体
    * */
   default void get(){
    System.out.println("我是默认方法");
    }

    /* 普通方法：jdk8新加支持普通方法写方法体，不需要子类实现，jdk8之前不支持
     * 静态方法，支持写方法体
     * */
    static void delete(){
        System.out.println("我是静态方法");
    }

}
