package com.example.demo.ctrl.test.java8;

/**
 * Lambda表达式练习
 * 教程：https://blog.csdn.net/weixin_34357436/article/details/93391227
 * https://www.runoob.com/java/java8-lambda-expressions.html
 */
public class LambdaTest {
    public static void main(String args[]){
        LambdaTest tester = new LambdaTest();//自定义类
        //MathOperation自定义接口
        // 类型声明MathOperation为自定义接口
        MathOperation addition = (int a, int b) -> a + b;

        // 不用类型声明
        MathOperation subtraction = (a, b) -> a - b;

        // 大括号中的返回语句
        MathOperation multiplication = (int a, int b) -> { return a * b; };

        // 没有大括号及返回语句
        MathOperation division = (int a, int b) -> a / b;
//        System.out.println((int a, int b) -> a + b);
        System.out.println("10 + 5 = " + tester.operate(10, 5, addition));
        System.out.println("10 - 5 = " + tester.operate(10, 5, subtraction));
        System.out.println("10 x 5 = " + tester.operate(10, 5, multiplication));
        System.out.println("10 / 5 = " + tester.operate(10, 5, division));

        // 不用括号
        GreetingService greetService1 = message ->
                System.out.println("Hello " + message);

        // 用括号
        GreetingService greetService2 = (message) ->
                System.out.println("Hello " + message);

        greetService1.sayMessage("Runoob");
        greetService2.sayMessage("Google");
    }


    //自定义接口
    interface MathOperation {
        int operation(int a, int b);
    }
    //自定义接口
    interface GreetingService {
        void sayMessage(String message);
    }
    //自定义方法
    private int operate(int a, int b, MathOperation mathOperation){
        return mathOperation.operation(a, b);
    }
}
