package com.example.demo.ctrl.test.java8;

public class AppleImpl implements Apple{
    //子类只需要重写抽象方法，不需要强制重写其他default和static方法
    public void add(){
        System.out.println("我是抽象方法");
    }
}
