package com.example.demo.ctrl.test.order;

import com.example.demo.ctrl.config.handler.OrderedHandler;
import com.example.demo.ctrl.dto.ContextBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
@Slf4j
public class BeanImplOne implements BeanInterface{
    @Autowired
    private OrderedHandler<ContextBean> handlerChain;

    public BeanImplOne(){
        log.info("进入BeanImplOne");
    }
}
