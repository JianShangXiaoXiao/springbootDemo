package com.example.demo.ctrl.test.java8;

import lombok.Data;
/**男人对象*/
@Data
public class ManDto {
    private String name;
    private int age;
    //无参数构造函数
    public ManDto(){

    }

    //有参数构造函数
    public ManDto(String name,int age){
        this.name=name;
        this.age=age;
    }
}
