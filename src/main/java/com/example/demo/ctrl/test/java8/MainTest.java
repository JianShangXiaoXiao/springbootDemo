package com.example.demo.ctrl.test.java8;

import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
/*java8新特性
教程;https://www.bilibili.com/video/BV1kK4y1p7M2?p=24&vd_source=3b76da7dbc7183616029fb52bb78f600
 *
 * */
public class MainTest {
    public static void main(String[] args) {
        MainTest m = new MainTest();
        Apple ap = new AppleImpl();
        //实现类只能调用到抽象方法和默认方法，不能调用到静态方法
        ap.add();
        ap.get();
        //静态方法需要用接口名才能调出来
        Apple.delete();

        /*方式一：匿名内部类调方法
        什么是匿名内部类:就是只有接口没有实现类，直接new一个子类并重写方法,这个子类没有名称，是匿名的。
        重写完后再调用这个方法让方法运行，否则方法没有运行仅仅是新建了对象。
        */
        new Bus() {
            @Override
            public void get() {
                System.out.println("我是匿名内部类的重写方法1");
            }
        }.get();
        /*
         * 方式二：先新建对象，再用对象调方法，这种方法新建了对象
         * 这种代码量更多一点
         * */
        Bus b = new Bus() {
            @Override
            public void get() {
                System.out.println("我是匿名内部类的重写方法2");
            }
        };
        b.get();
        /*
         * 方式三：Lambda 表达式调方法
         * 这种代码量最精简,简化我们匿名内部类的调用
         * */
        ((Bus) (() -> System.out.println("我是Lambda表达式的重写方法3"))).get();
        /*
         * 线程匿名类重写，Runnable底层就是函数接口
         * */
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("线程开始");
            }
        }
        ).start();
        //方法4,Lambda表达式调方法
        new Thread((() -> System.out.println("线程开始2"))).start();


        //------------------------------------------------
        //------------------------------------------------
        //-正式开始Lambda表达式练习-----------------------------------------------
        //无参数方法调用,返回值未知直接用接口名即可，()里代表入参，->代表分隔符，分隔符后面{}是实现类方法里面的内容（不包括方法名称），只有一条语句时可以省略{}
        Tea t = () -> System.out.println("Lambda调无参数方法");//新建匿名子对象
        t.get();//获取匿名子对象内容

        //有参数方法调用,返回值未知直接用接口名即可
        Tea2 t2 = (i, j) -> {
            return i + "-" + j;
        };//新建匿名子对象
        System.out.println("Lambda调有参数方法结果为：" + t2.get(1, 3));//获取匿名子对象内容

        //Lambda集合调用
        List<String> li = new ArrayList<String>();
        li.add("我");
        li.add("爱");
        li.add("你");
        //直接调匿名内部类遍历
        System.out.println("直接调匿名内部类遍历");
        //forEach方法参数是Consumer，而Consumer底层是函数接口
        li.forEach(new Consumer() {
            @Override
            public void accept(Object o) {
                System.out.println(o);
            }
        });
        //Lambda表达式遍历
        System.out.println("Lambda表达式遍历");
        li.forEach((o) -> System.out.println(o));

        //Lambda集合排序
        ArrayList<Integer> ul = new ArrayList<>();
        ul.add(4);
        ul.add(9);
        ul.add(1);
        System.out.println("排序前：" + ul.toString());
        //调用匿名类排序代码多，参数Comparator是接口函数
//        ul.sort(new Comparator<Integer>() {
//                    @Override
//                    public int compare(Integer i, Integer j) {
//                        return i - j;
//                    }
//                }
//        );
        System.out.println("第二次排序前：" + ul.toString());
        //Lambda排序代码少
        ul.sort((i, j) -> i - j);
        System.out.println("排序后：" + ul.toString());





        /*静态方法引入
         *引入外部方法到子类实现类的方法里面
         * */
        //直接用匿名内部类调用外部静态方法
        System.out.println("直接用匿名内部类调用外部静态方法");
        new Tea2() {
            @Override
            public String get(int i, int j) {
                MainTest.getStaticMethod(i, j);
                return "1";
            }
        }.get(1, 2);
        //Lambda静态方法引入
        Tea2 t3 = (i, j) -> {
            MainTest.getStaticMethod(i, j);
            return "1";
        };
        t3.get(4, 5);
        //Lambda静态方法引入，格式：类名::静态方法名
        Tea3 t4 = MainTest::getStaticMethod;//抽象方法get不能用返回值否则会报错
        t4.get(7, 4);



        /*实例方法引入
         *引入外部方法到子类实现类的方法里面
         * */
        Tea4 t5 = () -> {
            return "实例方法引入";
        };
        System.out.println(t5.get());
        //改变为Lambda表达式
        Tea4 t6 = () -> "实例方法引入2";
        System.out.println(t6.get());
        //老办法lambda表达式
        Tea4 t7 = () -> {
            return m.getStr();//引入普通方法,这里方法getStr并没有执行
        };
        t7.get();//这里才让方法getStr执行
        //新办法lambda表达式实例方法引入
        Tea4 t8 = m::getStr;
        t8.get();


        /*构造方法引入
         *引入外部方法到子类实现类的方法里面
         * */
        //lambda表达式老方法
        Tea5 t51 = () -> {
            return new ManDto("小明", 22);//用构造函数新建一个对象,并返回给接口函数
        };
        System.out.println("构造方法引入:" + t51.get());
        //lambda表达式新方法引入构造函数
        Tea5 t52 = ManDto::new;//只适合无参构造,格式特殊：出参对象::new
        System.out.println("构造方法引入:" + t52.get());

        /*对象方法引入
         *引入外部方法到子类实现类的方法里面
         * */
        //匿名方法调用方式
        Tea6 t61 = new Tea6() {
            @Override
            public String get(ManDto dto) {
                return dto.getName();
            }
        };
        System.out.println("对象方法引入:" + t61.get(new ManDto("小红", 11)));
        //lambda表达式老方式
        Tea6 t62 = (ManDto dto) -> dto.getName();
        System.out.println("对象方法引入:" + t62.get(new ManDto("小红2", 11)));
        //lambda表达式对象方法引入新方式
        Tea6 t63 = ManDto::getName;//格式：入参对象::对象方法
        System.out.println("对象方法引入:" + t63.get(new ManDto("小红3", 11)));


        /*Optional 类是一个可以为null的容器对象。如果值存在则isPresent()方法会返回true，调用get()方法会返回该对象。
        Optional 是个容器：它可以保存类型T的值，或者仅仅保存null。Optional提供很多有用的方法，这样我们就不用显式进行空值检测。
        Optional 类的引入很好的解决空指针异常。
        判断参数是否为空
        ofNullable(可以传递一个空对象)
        Of(不可以传递空对象)*/
        Integer a1 = 1;
        Integer a2 = null;
        Optional<Integer> a = Optional.ofNullable(a1);//是否存在
        System.out.println("是否存在" + a.isPresent() + ",值为：" + a.get());
//        Optional<Integer> a21 = Optional.ofNullable(a2);//是否存在,如果为空，返回null,然后获取get方法会报错,所以为空时要当心
//        System.out.println(Optional.ofNullable(a2)+"是否存在a2:"+a21.isPresent()+",值为："+a21.get());
        a2 = Optional.ofNullable(a2).orElse(10);//不存在则设置默认值
        System.out.println("默认值为：" + a2);
        //匿名类方式加过滤条件
        Integer a3 = null;
        Optional<Integer> a31 = Optional.ofNullable(a3);
        boolean isPresent = a31.filter(new Predicate<Integer>() {
                                           @Override
                                           public boolean test(Integer integer) {
                                               return integer < 20;//不为空的情况下增加一个过滤条件,为空的话不执行这个条件
                                           }
                                       }

        ).isPresent();
        System.out.println(isPresent);
        //lambda表达式方式加过滤条件：a33<20
        boolean isPresent2 = a31.filter(a33 -> a33 < 20).isPresent();
        System.out.println(isPresent2);

        //避免空指针写法
        // 优化前
        String mayiktName = "meite";
        if (mayiktName != null) {
            System.out.println(mayiktName);
        }
        //ifPresent与isPresent的区别，前者不会报空指针
        //优化后
        Optional<String> mayiktName2 = Optional.ofNullable(mayiktName);
//        // 当value 不为空时，则不会调用
//        mayiktName2.ifPresent(s -> System.out.println(s));
        mayiktName2.ifPresent(System.out::println);

        System.out.println("---分隔符------");

        ManDto order = null;
        ManDto order1 = m.getOrder(order);
        System.out.println(order1);

        /* 什么是stream流

        Stream 是JDK1.8 中处理集合的关键抽象概念，Lambda 和 Stream 是JDK1.8新增的函数式编程最有亮点的特性了，它可以指定你希望对集合进行的操作，可以执行非常复杂的查找、过滤和映射数据等操作。
        Stream创建方式：
        1：parallelStream为并行流采用多线程执行
        2：Stream采用单线程执行
        parallelStream效率比Stream要高。
        原理：
        Fork join 将一个大的任务拆分n多个小的子任务并行执行，
        最后在统计结果，有可能会非常消耗cpu的资源，确实可以
        提高效率。


        注意：数据量比较少的情况下，不要使用并行流。
        */

        ArrayList<ManDto> manList = new ArrayList<>();
        manList.add(new ManDto("mayikt", 20));
        manList.add(new ManDto("meite", 28));
        manList.add(new ManDto("zhangsan", 35));
        manList.add(new ManDto("xiaowei", 16));

//        manList.stream();
//        manList.parallelStream();

        //Stream将list转为set
        Stream<ManDto> mstr = manList.stream();//创建一个串行的stream流,List转换为Stream流
        Set mset = mstr.collect(Collectors.toSet());//stream转换为set
        System.out.println("set为：" + mset);
        //Stream将list转换为Map
        Stream<ManDto> mstr2 = manList.stream();//需要重新建一个流，不能重复使用
        Map<String, ManDto> map = mstr2.collect(Collectors.toMap(new Function<ManDto, String>() {
                                          @Override
                                          public String apply(ManDto userEntity) {//key
                                              String name = userEntity.getName();
                                              return name;
                                          }
                                      }, new Function<ManDto, ManDto>() {//value
                                          @Override
                                          public ManDto apply(ManDto t) {
                                              return t;
                                          }
                                      }
        ));


        map.forEach(new BiConsumer<String, ManDto>() {
            @Override
            public void accept(String s, ManDto userEntity) {
                System.out.println("遍历map:" + s + "," + userEntity.toString());
            }
        });


        //Stream将Reduce 求和
        Stream<Integer> integerStream = Stream.of(10, 30, 80, 60, 10, 70);//新建一个流
        //累加求和，依次加两个
        Optional<Integer> reduce = integerStream.reduce(new BinaryOperator<Integer>() {
            @Override
            public Integer apply(Integer a1, Integer a2) {
                return a1 + a2;
            }
        });
        System.out.println("求和结果："+reduce.get());

        Stream<Integer> integerStream2 = Stream.of(10, 30, 80, 60, 10, 70);//新建一个流
        Optional<Integer> reduce2 =integerStream2.reduce((aa1,aa2)->{
            return aa1+aa2;
        });
        System.out.println("求和结果2："+reduce2.get());

        Stream<Integer> integerStream3 = Stream.of(10, 30, 80, 60, 10, 70);//新建一个流
        //老方法
//                Optional<Integer> max = integerStream3.max(new Comparator<Integer>() {
//            @Override
//            public int compare(Integer o1, Integer o2) {
//                return o1 - o2;
//            }
//        });
        //新方法
        Optional<Integer> max =integerStream3.max((b1,b2)->b1-b2);
        System.out.println("最大值："+max.get());


        /*StreamMatch 匹配

        anyMatch表示，判断的条件里，任意一个元素成功，返回true
        allMatch表示，判断条件里的元素，所有的都是，返回true
        noneMatch跟allMatch相反，判断条件里的元素，所有的都不是，返回true*/
        Stream<Integer> integerStream4 = Stream.of(10, 30, 80, 60, 10, 70);//新建一个流
        boolean result =integerStream4.noneMatch((aa4)->aa4>60);//aa4是自定义变量，无数据类型
        System.out.println("都不是吗："+result);

        //Stream For循环
        Stream<Integer> integerStream5 = Stream.of(10, 30, 80, 60, 10, 70);//新建一个流
        integerStream5.forEach((aaa)->System.out.print(aaa+","));
        System.out.println("换行：");
        //Stream For排序
        Stream<Integer> integerStream6 = Stream.of(10, 30, 80, 60, 10, 70);//新建一个流
        //先排序，在遍历打印
        integerStream6.sorted((bb1,bb2)->bb1-bb2).forEach((cc)->System.out.print(cc+","));
        /*Limit 获取第一个
        Skip 就是跳过*/
        System.out.println("换行：");
        Stream<Integer> integerStream7 = Stream.of(10, 30, 80, 60, 10, 70);//新建一个流
        integerStream7.skip(2).limit(1).forEach(dd -> System.out.print(dd+","));


    }


    /**
     * 静态方法引入
     */
    public static void getStaticMethod(int a, int b) {
        System.out.println("我是静态方法引入" + a + "," + b);
    }

    private static ManDto get() {
        return new ManDto("小明", 22);//用构造函数新建一个对象,并返回给接口函数
    }

    /**
     * 实例方法引入
     */
    public String getStr() {
        System.out.println("我是实例方法引入");
        return "我是实例方法引入";
    }

    private static ManDto createOrder() {
        return new ManDto("小刚", 33);
    }

    public static ManDto getOrder(ManDto order) {
        // 优化前
//        if (order == null) {
//            return createOrder();
//        }
//        return order;
        // 优化后
//        return Optional.ofNullable(order).orElseGet(new Supplier<ManDto>() {
//            @Override
//            public ManDto get() {
//                return createOrder();
//            }
//        });
        //再次优化
        return Optional.ofNullable(order).orElseGet(() -> createOrder());
    }
}
