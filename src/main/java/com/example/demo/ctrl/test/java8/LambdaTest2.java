package com.example.demo.ctrl.test.java8;
/**
 * Lambda表达式练习
 * 教程：https://blog.csdn.net/weixin_34357436/article/details/93391227
 * https://blog.csdn.net/qq_36561697/article/details/80847812
 * Lambda表达式的语法
 * 基本语法:
 * (parameters) -> expression
 * 或
 * (parameters) ->{ statements; }
 */
public class LambdaTest2 {

    public static void main(String[] args) {
System.out.println();
        // 1. 不需要参数,返回值为 5, () -> 5


// 2. 接收一个参数(数字类型),返回其2倍的值 x -> 2 * x

// 3. 接受2个参数(数字),并返回他们的差值 (x, y) -> x – y

// 4. 接收2个int型整数,返回他们的和 (int x, int y) -> x + y

// 5. 接受一个 string 对象,并在控制台打印,不返回任何值(看起来像是返回void) (String s) -> System.out.print(s)

    }

    interface MathOperation {
        int operation(int a, int b);
    }
}
