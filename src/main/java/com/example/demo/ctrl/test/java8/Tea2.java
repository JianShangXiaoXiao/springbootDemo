package com.example.demo.ctrl.test.java8;
/**
 函数接口
 @FunctionalInterface   标记为该接口为函数接口,在该接口中只能存在一个抽象方法,多了抽象方法会报错。
 保证一个抽象方法，方便使用Lambda 表达式。
 函数接口定义
 1.在接口中只能有一个抽象方法
 2.@FunctionalInterface   标记为该接口为函数接口
 3.可以通过default 修饰为普通方法
 4.可以定义object类中的方法
 */
@FunctionalInterface
public interface Tea2 {
    //抽象方法
    String get(int i,int j);
}
