package com.example.demo.ctrl.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.ctrl.config.annotation.HandlerComponent;
import com.example.demo.ctrl.config.bean.beanConfig.PropertiesConfig;
import com.example.demo.ctrl.config.handler.OrderedHandler;
import com.example.demo.ctrl.config.http.feign.UserService;
import com.example.demo.ctrl.config.http.restTemplate.HttpClient;
import com.example.demo.ctrl.config.http.restTemplate.PortalHttpRequest;
import com.example.demo.ctrl.config.redis.RedisService;
import com.example.demo.ctrl.config.threadTest.Thread.ReadThread;
import com.example.demo.ctrl.dto.Author;
import com.example.demo.ctrl.dto.ContextBean;
import com.example.demo.ctrl.dto.HandlerConstants;
import com.example.demo.ctrl.exception.BusinessException;
import com.example.demo.ctrl.exception.CommonResultCode;
import com.example.demo.ctrl.test.order.BeanInterface;
import com.example.demo.ctrl.util.LogToString;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.condition.PatternsRequestCondition;
import org.springframework.web.servlet.mvc.condition.RequestMethodsRequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/**
 * 返回字符串
 */
@RestController
@Slf4j
//@ResponseBody //支持将返回值放在response体内，而不是返回一个页面
public class DemoController {
    @Autowired
    private HttpClient httpClientImpl;
    //引入java方式配置的bean,默认为Bean方法名
    @Autowired
    private HttpClient icoreClient;
    //fegin调接口测试
    @Autowired
    private UserService userService;
//    @Autowired
//    private DbMapper dbMapper;
    //获取java配置项
    @Autowired
    private PropertiesConfig propertiesConfig;
    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    @Autowired
    private RedisService redisService;
    @Autowired
    private List<BeanInterface> beanList;
    @Autowired
    private Map<String, BeanInterface> beanMap;

    @Autowired
    private  OrderedHandler<ContextBean> handlerChain;
    //获取properties配置项http.config.icore.hostUrl，冒号后面指定默认值
    @Value("${http.config.icore.hostUrl:默认值} : 默认值" )
    private String icoreUrl;

    public static String ss="3333";
    /**
     * bean容器
     */
    @Autowired
    private ApplicationContext applicationContext;
    /**容器*/
    @Autowired
    private WebApplicationContext weAapplicationContext;
    /** 统计所有接口 */
    @PostConstruct
    public void urls() {
        log.info("weAapplicationContext容器为："+weAapplicationContext);
        RequestMappingHandlerMapping mapping = applicationContext.getBean(RequestMappingHandlerMapping.class);
//        applicationContext.g
        String[] beans=applicationContext.getBeanDefinitionNames();
        log.info("所有beans:"+beans+",长度："+beans.length);
        for (String beanName : beans) {
//            if(!beanName.startsWith("com.example.demo")){
//                continue;
//            }



            Class<?> beanType = applicationContext.getType(beanName);
            System.out.println("BeanName:" + beanName);
            System.out.println("Bean的类型：" + beanType);
            System.out.println("Bean所在的包：" + beanType.getPackage());
            System.out.println("Bean：" + applicationContext.getBean(beanName));

//            break;
        }


        // 获取url与类和方法的对应信息
        Map<RequestMappingInfo, HandlerMethod> map = mapping.getHandlerMethods();
        List<Map<String, String>> list = new ArrayList<Map<String, String>>();
        List<String> urlList = new ArrayList<String>();
        String urlStr="";
        for (Map.Entry<RequestMappingInfo, HandlerMethod> m : map.entrySet()) {
            Map<String, String> map1 = new HashMap<String, String>();
            RequestMappingInfo info = m.getKey();
            HandlerMethod method = m.getValue();
            PatternsRequestCondition p = info.getPatternsCondition();
            for (String url : p.getPatterns()) {
                map1.put("url", url);
                urlStr=urlStr+url+",";
            }
            map1.put("className", method.getMethod().getDeclaringClass().getName()); // 类名
            RequestMethodsRequestCondition methodsCondition = info.getMethodsCondition();
            for (RequestMethod requestMethod : methodsCondition.getMethods()) {
                map1.put("type", requestMethod.toString());
            }
            list.add(map1);
        }
        log.info("接口"+ LogToString.objectToString(list));
        log.info("所有url:"+urlStr);
        log.info("换行:"+"\n"+"换行成功");
    }


    /**
     * Oracle连接
     * @return
     */
//    @RequestMapping("/oracle")
//    public String oracle(){
//        String id=dbMapper.queryId();
//        log.info("id为:"+id);
//        return id;
//    }


    /**
     * http调接口测试>..
     * @return
     */
    @RequestMapping("/httpTest")
    public String httpTest(){
        JSONObject json=null;
        Map<String,Object> map=new HashMap<String,Object>();
        map.put("id","12345");
        PortalHttpRequest portalHttpRequest=PortalHttpRequest.buildPost("http://localhost:8080/getName", map);
        portalHttpRequest.setMediaType(MediaType.APPLICATION_JSON_UTF8);
        String result=httpClientImpl.exec(portalHttpRequest,String.class);
        log.info("http调接口测试出参："+result);
        //fegin调接口方式
        String result2=userService.getUserinfo("11223");
        log.info("fegin调接口测试出参："+result2);
        return result;
    }


    @GetMapping(value = "/getName")
    public String getName(){

        log.info("获取properties配置项http.config.icore.hostUrl："+icoreUrl);
        String name=(String)(propertiesConfig.getUtil().getOrDefault("name","名字"));
        log.info("获取properties配置项nme:"+name);
        return "我的名字是小明！";
    }

    /**
     * 抛异常练习
     * @return
     */
    @RequestMapping(value = "/exception")
    public String exception(){
        throw new BusinessException(CommonResultCode.LOGIN_FAIL);
    }

    /**
     * 测试redis
     * @return
     */

    @RequestMapping("/redisTest")
    public String redisTest() throws InterruptedException {
        ValueOperations<String,String> redis=redisTemplate.opsForValue();
        redis.set("name","刘晓晓");
        redis.set("age","22");
        //设置失效时间
        redis.set("aaaa","我的名字是小米",1, TimeUnit.SECONDS);
        Thread.sleep(60000);
        String re=(String)redis.get("name");
        String aaaa=(String)redis.get("aaaa");
        log.info("redis取出结果："+re+"失效值："+aaaa);
        return  "Redis调用成功"+re;
    }

    /**
     * 测试redis
     * @return
     */

    @RequestMapping("/redisTest2")
    public String redisTest2() throws InterruptedException {
        String mobile="13314567896";
        Author a=new Author();
        a.setUid("123456789");
        a.setMobile(mobile);
        redisService.getAuthorInfo2(mobile,a);
        return  "Redis2调用成功";
    }

    /**
     * 排序练习，@Order,一般用@Order对list排序
     * 可以看到list的数据反过来了，但是map的数据却没有变。
     * 因为@Order针对数组的。
     * 注解@Order或者接口Ordered的作用是定义Spring IOC容器中Bean的执行顺序的优先级，而不是定义Bean的加载顺序，Bean的加载顺序不受@Order或Ordered接口的影响
     */
    @RequestMapping("/orderTest")
    public String orderTest(){
        if(!CollectionUtils.isEmpty(beanList)) {
            System.out.println("beanList为 : "+beanList);
            for (BeanInterface bean : beanList) {
                System.out.println(bean.getClass().getName());
            }
        }

        if(!CollectionUtils.isEmpty(beanMap)) {
            System.out.println("beanMap为 : "+beanMap);
            for (Map.Entry<String, BeanInterface> entry : beanMap.entrySet()) {
                System.out.println(entry.getKey() + "  " + entry.getValue().getClass().getName());
            }
        }
        return  "排序练习成功";
    }


    /**
     * 容器练习
     * @return
     */
    @RequestMapping(value = "/getContainer")
    public String getContainer(){
        log.info("bean容器练习");
        log.info(applicationContext.getApplicationName());
        log.info("获取"+applicationContext.getBean("icoreClient"));
        log.info("包含"+applicationContext.containsBean("icoreClient"));
        //获取所有bean
        String[] beanNames=applicationContext.getBeanDefinitionNames();
        for (int i = 0; i <beanNames.length ; i++) {
            log.info("所有beanNames:"+beanNames[i]);
        }
        log.info("bean容器练习");

        return "bean容器练习！";
    }

    /**
     * 打日志练习
     * @return
     */
    @RequestMapping(value = "/logTest")
    public String logTest(){
        log.info("主方法");
        HashMap<Integer, Integer> hashMap = new HashMap();
        /** 多线程编辑100次*/
        for (int i = 0; i < 2; i++) {
            new Thread(new ReadThread(MDC.get("F"))).start();
        }


        return "打多线程日志练习";
    }

    /**
     * handler练习，策略模式应用
     * @return
     */
    @RequestMapping(value = "/handler")
    @HandlerComponent(threadHandler= HandlerConstants.handler) //策略模式应用,触发切面
    public String handler(){
        ContextBean t=ContextBean.getThreadContextBean();
        log.info("请求入参："+t);
        //开始执行handler链
        handlerChain.handle(t);
        return "handler练习";
    }
}
