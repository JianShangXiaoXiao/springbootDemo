package com.example.demo.ctrl.dto;

import com.example.demo.ctrl.handler.LoginHandler;
import com.example.demo.ctrl.handler.WXLoginHandler;

import java.util.HashMap;
import java.util.Map;

/**
 * handler调用顺序记录
 */
public class HandlerConstants {
    public  static final String handler="testHandler";
    public  static final Map<String,Integer> handlerOrderBy=new HashMap<String,Integer>();
    static {
        handlerOrderBy.put(LoginHandler.class.getSimpleName(),1);
        handlerOrderBy.put(WXLoginHandler.class.getSimpleName(),2);
    }

}
