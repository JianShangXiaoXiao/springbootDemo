package com.example.demo.ctrl.db.mapper;

import org.springframework.stereotype.Component;

/**数据库连接 */
@Component
public interface DbMapper {
    String queryId();
}
