package com.example.demo.ctrl.config.http.feign;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * springcloud的feign调接口，比httpclient方便
 */
@FeignClient(name = "provider",url="http://localhost:8080") //指定调用对象实体,这里如果不引入eureka需要指定url才不会报错，否则会报错需要引入eureka包
public interface UserService {

    @RequestMapping(value = "/getName",method = RequestMethod.GET)
    public String getUserinfo(@RequestParam(value = "userid") String userid);

}