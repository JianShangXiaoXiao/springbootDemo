package com.example.demo.ctrl.config.bean.beanConfig;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

@Data
@Component
@ConfigurationProperties(prefix = "online")
public class PropertiesConfig {
    private Map<String,Object> util;

}
