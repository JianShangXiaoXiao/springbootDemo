package com.example.demo.ctrl.config.http.restTemplate;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

/**
 * http连接配置
 */
@Setter
@Getter
@ToString
@Slf4j
public class HttpConfig {
    /**
     * 主机url
     */
    private String hostUrl;
    //重试次数
    private int retryTime;
    //连接超时时间
    private int connectionTimeOut;
    //读取超时时间
    private int socketTimeOut;
    //同路由并发数
    private int maxPerRoute;
    //配置最大连接数
    private int maxTotal;
    /**
     * 默认构造函数
     */
    public  HttpConfig(){
        retryTime=0;
        maxPerRoute=200;
        maxTotal=200;
        connectionTimeOut=6000;
        socketTimeOut=6000;
    }


}
