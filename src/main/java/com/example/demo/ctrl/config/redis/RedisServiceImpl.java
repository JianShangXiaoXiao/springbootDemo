package com.example.demo.ctrl.config.redis;

import com.example.demo.ctrl.dto.Author;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

/**
 * 测试redis
 */
@Service
@Slf4j
public class RedisServiceImpl implements RedisService{
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    public static void main(String[] args) {
        //连接本地的 Redis 服务
        Jedis jedis = new Jedis("localhost");
        System.out.println("连接成功");
        //查看服务是否运行
        System.out.println("服务正在运行: "+jedis.ping());
    }
    @Override
    public Author getAuthorInfo2(String mobile,Author a){
        log.info("放入缓存："+a);
        return getAuthorInfo(mobile,a);
    }
    /**
     * 缓存登录信息，并获取缓存后的新数据,教程：https://blog.csdn.net/zzhongcy/article/details/100543263
     * 使用@Cacheable操作缓存信息，可缓存方法返回值
     *@Cacheable可以标记在一个方法上，也可以标记在一个类上。当标记在一个方法上时表示该方法是支持缓存的，当标记在一个类上时则表示该类所有的方法都是支持缓存的
     *
     * Spring为我们提供了几个注解来支持Spring Cache。其核心主要是@Cacheable和@CacheEvict。使用@Cacheable标记的方法在执行后Spring Cache将缓存其返回结果，而使用@CacheEvict标记的方法会在方法执行前或者执行后移除Spring Cache中的某些元素。下面我们将来详细介绍一下Spring基于注解对Cache的支持所提供的几个注解。
     */
    @Cacheable(cacheNames = "authorInfo",key="authorInfo",unless = "#result==null")
    public Author getAuthorInfo(String mobile,Author a){
        return a;
    }

}
