package com.example.demo.ctrl.config.threadTest.Thread;

import com.example.demo.ctrl.util.IDKeyUtil;
import com.example.demo.ctrl.util.MDCUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 线程类
 */
@Slf4j
public class ReadThread implements Runnable {

    Map<Integer, Integer> hashMap;
    int i;
    String s;
    public ReadThread(Map<Integer, Integer> hashMap) {
        this.hashMap = hashMap;
    }
    public ReadThread(int i) {
        this.i = i;
    }
    public ReadThread(String s) {
        this.s = s;
    }
    @Override
    public void run() {
        MDCUtil.setFlowId(s);
        MDCUtil.setActionId(IDKeyUtil.generateId());
        log.info("开始读value " );
        log.info("结束读: " + i);
        MDCUtil.removeActionId();
    }
}
