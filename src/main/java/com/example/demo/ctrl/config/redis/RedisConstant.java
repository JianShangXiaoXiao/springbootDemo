package com.example.demo.ctrl.config.redis;

public class RedisConstant {
    /**
     * 在进行缓存数据与数据库数据同步时每次更新1000条,防止SQL语句拼接超长更新失败以及数据过多更新较慢问题
     */
    public static final Integer DEFAULT_SYNC_LENGTH = 1000;
    /**
     * 默认数据库中存储数据长度为200,后期根据数据库内存使用情况调整(调整步频为200)
     */
    public static final Integer DEFAULT_MAX_LENGTH = 2000;
    /**
     * 数据自增步长为1
     */
    public static final Integer INCREMENT = 1;
    /**
     * 数据自减步长为1
     */
    public static final Integer REDUCTION = -1;

    public static final String CATEGORY ="";
    /**
     * 存储用户信息key
     * 此处只放USER一个常量，这个常量类的作用：全局使用，一改全改，注意，如果你不想麻烦就一定要将USER常量的:留着，对于后续操作缓存提供了太多的方便！！
     */
    public static final String USER = "user:";
}
