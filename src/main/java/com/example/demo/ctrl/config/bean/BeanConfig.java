package com.example.demo.ctrl.config.bean;

import com.example.demo.ctrl.config.http.restTemplate.HttpClient;
import com.example.demo.ctrl.config.http.restTemplate.HttpClientImpl;
import com.example.demo.ctrl.config.http.restTemplate.HttpConfig;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Java配置方式注入bean
 */
//声明这是个配置类
@Configuration
//引入properties文件通过前缀找到配置项
@ConfigurationProperties(prefix = "http.config")
@Data
public class BeanConfig {
    //配置项http.config.icore
    private HttpConfig icore;
    //配置项http.config.pnbs
    private HttpConfig pnbs;
    /**
     * java配置方式注入bean,适用于不同配置参数的同一个对象，需要配置参数的对象,不需要配置参数的对象不需要这样做
     * @return
     */
    @Bean
    public HttpClient icoreClient(){
        HttpClient h=new HttpClientImpl(icore);
        return h;
    }

    @Bean
    public HttpClient pnbsClient(){
        HttpClient h=new HttpClientImpl(pnbs);
        return h;
    }
}
