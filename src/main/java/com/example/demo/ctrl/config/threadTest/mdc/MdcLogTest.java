package com.example.demo.ctrl.config.threadTest.mdc;

import com.example.demo.ctrl.config.threadTest.Thread.ReadThread;
import com.example.demo.ctrl.util.MDCUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;

/**
 * 多线程高并发如何打日志
 */
@Slf4j
public class MdcLogTest {
    public static void main(String[] args) {
        MDCUtil.setFlowId("33333333");
        log.info("主方法");
        HashMap<Integer, Integer> hashMap = new HashMap();
        /** 多线程编辑100次*/
        for (int i = 0; i < 20; i++) {
            new Thread(new ReadThread(i)).start();
        }

        MDCUtil.removeFlowId();
    }

}
