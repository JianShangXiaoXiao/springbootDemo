package com.example.demo.ctrl.handler;

import com.example.demo.ctrl.config.annotation.HandlerComponent;
import com.example.demo.ctrl.config.handler.OrderedHandler;
import com.example.demo.ctrl.dto.ContextBean;
import com.example.demo.ctrl.dto.HandlerConstants;
import lombok.extern.slf4j.Slf4j;

/**
 * 普通登录校验
 */
@Slf4j
@HandlerComponent(HandlerConstants.handler)
public class LoginHandler implements OrderedHandler<ContextBean> {
    @Override
    public boolean isEligible(ContextBean contextBean) {
        return true;//true:标识执行下面的handle方法，false:不执行
    }

    @Override
    public void handle(ContextBean contextBean) {
        log.info("进入LoginHandler处理逻辑");
    }

    /**
     * 指定执行顺序
     * @return
     */
    @Override
    public int getOrder() {
        return 1;
    }
}
